import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { PostsAPI } from 'service/PostsAPI'
import PostWidget from './PostWidget'

const PostsWidget = ({ userId, isProfile = false }) => {
	const dispatch = useDispatch()

	const posts = useSelector(state => state.posts)
	const token = useSelector(state => state.token)
	const { getPosts, getUserPosts } = new PostsAPI(dispatch)

	useEffect(() => {
		if (isProfile) {
			getUserPosts(userId, token)
		} else {
			getPosts(token)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<>
			{posts.map(
				({
					_id,
					userId,
					firstName,
					lastName,
					description,
					location,
					picturePath,
					userPicturePath,
					likes,
					comments,
				}) => (
					<PostWidget
						key={_id}
						postId={_id}
						postUserId={userId}
						name={`${firstName} ${lastName}`}
						description={description}
						location={location}
						picturePath={picturePath}
						userPicturePath={userPicturePath}
						likes={likes}
						comments={comments}
					/>
				)
			)}
		</>
	)
}

export default PostsWidget
