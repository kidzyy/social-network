export class UserAPI {
	async getUser(userId, token, setUser) {
		const response = await fetch(`http://localhost:3001/users/${userId}`, {
			method: 'GET',
			headers: { Authorization: `Bearer ${token}` },
		})
		const data = await response.json()
		setUser(data)
	}
}
