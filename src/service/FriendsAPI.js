import { setFriends } from 'state'

export class FriendsAPI {
	static dispatch

	constructor(dispatch) {
		FriendsAPI.dispatch = dispatch
	}

	async patchFriend(friendId, _id, token) {
		const response = await fetch(
			`http://localhost:3001/users/${_id}/${friendId}`,
			{
				method: 'PATCH',
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json',
				},
			}
		)
		const data = await response.json()

		FriendsAPI.dispatch(setFriends({ friends: data }))
	}

	async getFriends(userId, token) {
		const response = await fetch(
			`http://localhost:3001/users/${userId}/friends`,
			{
				method: 'GET',
				headers: { Authorization: `Bearer ${token}` },
			}
		)
		const data = await response.json()
		FriendsAPI.dispatch(setFriends({ friends: data }))
	}
}
