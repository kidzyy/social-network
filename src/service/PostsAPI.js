import { setPost, setPosts } from 'state'

class DispatchPostsAPI {
	static dispatch

	constructor(dispatch) {
		PostsAPI.dispatch = dispatch
	}
}

export class PostsAPI extends DispatchPostsAPI {
	async getPosts(token) {
		const response = await fetch('http://localhost:3001/posts', {
			method: 'GET',
			headers: { Authorization: `Bearer ${token}` },
		})
		const data = await response.json()
		PostsAPI.dispatch(setPosts({ posts: data }))
	}

	async getUserPosts(userId, token) {
		const response = await fetch(
			`http://localhost:3001/posts/${userId}/posts`,
			{
				method: 'GET',
				headers: { Authorization: `Bearer ${token}` },
			}
		)
		const data = await response.json()
		PostsAPI.dispatch(setPosts({ posts: data }))
	}

	async patchLike(postId, token, loggedInUserId) {
		const response = await fetch(`http://localhost:3001/posts/${postId}/like`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ userId: loggedInUserId }),
		})
		const updatedPost = await response.json()
		PostsAPI.dispatch(setPost({ post: updatedPost }))
	}
}

export class PostsHandleForm extends DispatchPostsAPI {
	async handlePost(_id, post, image, token, setImage, setPost) {
		const formData = new FormData()
		formData.append('userId', _id)
		formData.append('description', post)
		if (image) {
			formData.append('picture', image)
			formData.append('picturePath', image.name)
		}

		const response = await fetch(`http://localhost:3001/posts`, {
			method: 'POST',
			headers: { Authorization: `Bearer ${token}` },
			body: formData,
		})
		const posts = await response.json()
		PostsAPI.dispatch(setPosts({ posts }))
		setImage(null)
		setPost('')
	}
}
